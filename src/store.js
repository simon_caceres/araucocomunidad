import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    title: 'Parque Arauco',
    data: undefined
  },
  mutations: {
    setData(state, payload) {
      //la idea es manejar la data global como los establecimientos y los paises desde el store, para con una sola llamada derivar a cada una de las vistas.
      state.data = payload;
    }
  },
  actions: {
    // async getData() {
    //   let response
  
    //   try {
    //       response = await Vue.http.get('localhost:3000/api/establishments')
    //       console.log(response)
    //   } catch (ex) {
    //       // Handle error
    //       return
    //   }
  
    //   // Handle success
    //   const data = response.body
    //   console.log(data)
    // }
  }
})
